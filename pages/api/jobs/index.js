import { jobs } from "../../../data";

export default async (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      res.status(200).json(jobs);
      break;
    case "POST":
      saveJob(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Metodo ${method} no permitido`);
  }
};

const saveJob = (req, res) => {
  const {
    query: { title, company, location },
  } = req;

  let job = {
    id: Math.floor(Math.random() * 16) + 5,
    title,
    company,
    location,
  };

  if (!title || !company || !location) {
    return res.status(404).json({
      message: `Los campos title, company y location son obligatorios`,
    });
  }

  jobs.push(job);

  return res.status(200).json(jobs);
};
